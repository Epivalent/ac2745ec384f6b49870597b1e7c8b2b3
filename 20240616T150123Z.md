In the 1980s in the USSR, there were several reforms and policies introduced that aimed to legalize private factories and encourage limited entrepreneurship. The specific time period and corresponding documents outlining these changes are as follows:

1. Time Period: Late 1980s (specifically from 1987 onwards)
   - During this time, under the leadership of Mikhail Gorbachev, the Soviet Union witnessed a series of economic reforms known as perestroika (restructuring) and glasnost (openness). These reforms aimed to revitalize the stagnant Soviet economy by introducing elements of market-oriented policies.

2. Document: Law on State Enterprises
   - In June 1987, the Law on State Enterprises was passed by the Supreme Soviet of the USSR. This law allowed state-owned enterprises to have more autonomy in decision-making processes, including setting prices for their products/services and engaging in limited profit-making activities.

3. Document: Law on Cooperatives
   - In May 1988, another significant document called the Law on Cooperatives was enacted. This law legalized small-scale private businesses known as cooperatives, which could operate in various sectors such as manufacturing, services, agriculture, etc. It provided legal grounds for individuals or groups to establish their own enterprises outside of state control.

4. Document: Decree No. 1400
   - In December 1988, Decree No. 1400 was issued by the Council of Ministers of the USSR. This decree further expanded opportunities for private enterprise by allowing individuals or groups to lease state-owned facilities or land for entrepreneurial purposes.

It is important to note that while these documents outlined measures towards private factory legalization and entrepreneurship promotion during this period, they did not lead to a complete transition towards a market economy or full-scale privatization in the USSR before its dissolution in 1991.
# Sun 16 Jun 15:01:23 CEST 2024 - at 1980s in USSR, what was the exact time period and the names of corresponding documents outlining the private factories legalization